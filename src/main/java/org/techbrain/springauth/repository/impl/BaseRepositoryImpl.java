package org.techbrain.springauth.repository.impl;

import org.bson.types.ObjectId;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.techbrain.springauth.config.CollectionName;
import org.techbrain.springauth.model.IdentityModel;
import org.techbrain.springauth.repository.BaseRepository;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;


public class BaseRepositoryImpl<T extends IdentityModel> implements BaseRepository<T> {

    protected MongoCollection coll;
    protected Class<T> entity;
    @Autowired
    private transient Jongo jongo;

    @PostConstruct
    public void init() {
        try {
            Type t = this.getClass().getGenericSuperclass();
            ParameterizedType pt = (ParameterizedType) t;
            this.entity = (Class<T>) pt.getActualTypeArguments()[0];
            this.coll = jongo.getCollection(entity.getAnnotation(CollectionName.class).name());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public T save(T obj) {
        if (obj != null) {
            obj.prepare();
            Object upsertedId = coll.save(obj).getUpsertedId();
            if (upsertedId != null && upsertedId instanceof ObjectId) {
                obj.setId(String.valueOf(upsertedId));
                return obj;
            }
        }
        return null;
    }

    @Override
    public T update(String id, T obj) {
        return null;
    }

    @Override
    public T get(String id) {
        return null;
    }

    @Override
    public List<T> get(int start, int limit) {
        return null;
    }

    @Override
    public List<T> listAll() {
        return null;
    }

    @Override
    public void delete(String id) {

    }
}
