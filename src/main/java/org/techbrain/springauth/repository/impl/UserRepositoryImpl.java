package org.techbrain.springauth.repository.impl;


import org.springframework.stereotype.Repository;
import org.techbrain.springauth.model.User;
import org.techbrain.springauth.repository.UserRepository;

@Repository
public class UserRepositoryImpl extends BaseRepositoryImpl<User> implements UserRepository {
    @Override
    public User findByEmail(String email) {
        return this.coll.findOne("{deleted:#, email:#}", Boolean.FALSE, email).as(this.entity);
    }

    @Override
    public User findByUserName(String userName) {
        return this.coll.findOne("{deleted:#, userName:#}", Boolean.FALSE, userName).as(this.entity);
    }
}
