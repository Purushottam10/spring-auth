package org.techbrain.springauth.repository;

import org.techbrain.springauth.model.IdentityModel;

import java.util.List;

public interface BaseRepository<T extends IdentityModel> {
    T save(T obj);

    T update(String id, T obj);

    T get(String id);

    List<T> get(int start, int limit);

    List<T> listAll();

    void delete(String id);
}
