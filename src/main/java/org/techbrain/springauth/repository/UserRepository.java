package org.techbrain.springauth.repository;

import org.techbrain.springauth.model.User;

public interface UserRepository extends BaseRepository<User>{
    User findByEmail(String email);

    User findByUserName(String userName);
}
