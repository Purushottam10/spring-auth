package org.techbrain.springauth.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.techbrain.springauth.model.User;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class AuthResponse  implements Serializable {
    private  String jwtToken;
    private User user;
}
