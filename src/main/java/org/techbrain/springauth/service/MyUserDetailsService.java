package org.techbrain.springauth.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.techbrain.springauth.model.User;
import org.techbrain.springauth.request.AuthRequest;
import org.techbrain.springauth.request.UserRequest;
import org.techbrain.springauth.response.AuthResponse;

public interface MyUserDetailsService extends UserDetailsService {
    AuthResponse createToken(AuthRequest authRequest);

    User findByEmail(String s);

    User findByUserName(String userName);

    User save(UserRequest newUser);
}
