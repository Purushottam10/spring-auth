package org.techbrain.springauth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.techbrain.springauth.repository.UserRepository;
import org.techbrain.springauth.request.AuthRequest;
import org.techbrain.springauth.request.UserRequest;
import org.techbrain.springauth.response.AuthResponse;
import org.techbrain.springauth.service.MyUserDetailsService;
import org.techbrain.springauth.util.JwtUtil;

import java.util.ArrayList;

@Service
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserRepository userRepository;
//    @Autowired
//    private BCryptPasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        org.techbrain.springauth.model.User user = findByUserName(userName);
        return new User(user.getUserName(),user.getPassword(), new ArrayList<>());
    }

    @Override
    public AuthResponse createToken(AuthRequest authRequest) {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            throw new RuntimeException("Bad Request");
        }
        final org.techbrain.springauth.model.User userDetails = findByUserName(authRequest.getUserName());

        final String jwt = jwtUtil.generateToken(userDetails);
        AuthResponse authResponse = new AuthResponse();
        authResponse.setJwtToken(jwt);
        authResponse.setUser(userDetails);
        return authResponse;
    }

    @Override
    public org.techbrain.springauth.model.User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public org.techbrain.springauth.model.User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
    @Override
    public org.techbrain.springauth.model.User save(UserRequest newUser) {
        return userRepository.save(prepare(newUser));
    }

   private org.techbrain.springauth.model.User prepare(UserRequest userRequest){

        org.techbrain.springauth.model.User user = new org.techbrain.springauth.model.User();
        user.setUserName(userRequest.getUserName());
        user.setEmail(userRequest.getEmail());
        user.setPassword(userRequest.getPassword());
        return user;
    }
}