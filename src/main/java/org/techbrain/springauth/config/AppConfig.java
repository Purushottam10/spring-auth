package org.techbrain.springauth.config;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;
import org.jongo.Jongo;
import org.jongo.marshall.jackson.JacksonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class AppConfig {

    private Logger logger = LoggerFactory.getLogger(AppConfig.class);

   @Autowired
   MongoProperties mongoProperties;
    /**
     * @return object of jongo class
     */
    @Bean
    public Jongo getDbConfig() {
        try {
            String uri = mongoProperties.getUri();
            logger.warn("uri : {}", uri);
            MongoClientURI clientURI = new MongoClientURI(uri);
            MongoClient client = new MongoClient(new MongoClientURI(uri));
            DB db = new DB(client, client.getDatabase(Objects.requireNonNull(clientURI.getDatabase())).getName());
            db.setWriteConcern(WriteConcern.ACKNOWLEDGED);
            return new Jongo(db,
                    new JacksonMapper.Builder()
                            .registerModule(new JodaModule())
                            .enable(MapperFeature.AUTO_DETECT_GETTERS)
                            .build());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
