package org.techbrain.springauth.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppProperties {

}
