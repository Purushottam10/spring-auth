package org.techbrain.springauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.techbrain.springauth.request.AuthRequest;
import org.techbrain.springauth.request.UserRequest;
import org.techbrain.springauth.service.MyUserDetailsService;

@RestController
@RequestMapping("/user")
public class UserController {

    private MyUserDetailsService myUserDetailsService;

    @Autowired
    public UserController(MyUserDetailsService myUserDetailsService) {
        this.myUserDetailsService = myUserDetailsService;
    }

    @GetMapping("/status")
    public String status(){
        return "up";
    }

    @PostMapping("/auth")
    public ResponseEntity createToken(@RequestBody AuthRequest authRequest){
        try{
            return ResponseEntity.ok(myUserDetailsService.createToken(authRequest));
        } catch (Exception ex){
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

    @PostMapping("/save")
    public ResponseEntity create(@RequestBody UserRequest userRequest){
        try{
            return ResponseEntity.ok(myUserDetailsService.save(userRequest));
        } catch (Exception ex){
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

}
