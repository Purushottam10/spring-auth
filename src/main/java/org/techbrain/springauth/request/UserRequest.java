package org.techbrain.springauth.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRequest {
    private String userName;
    private String password;
    private String email;
}
