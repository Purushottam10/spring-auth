package org.techbrain.springauth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.techbrain.springauth.model.User;
import org.techbrain.springauth.request.UserRequest;
import org.techbrain.springauth.service.MyUserDetailsService;

import java.util.Objects;

@SpringBootApplication
public class SpringAuthApplication extends SpringBootServletInitializer {
    private static final Logger LOGGER = LogManager.getLogger(SpringAuthApplication.class);
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringAuthApplication.class, args);
        MyUserDetailsService userService = applicationContext.getBean(MyUserDetailsService.class);
        User user = userService.findByEmail("puru@gmail.com");
        if (Objects.isNull(user)) {
            //BCryptPasswordEncoder passwordEncoder = applicationContext.getBean(BCryptPasswordEncoder.class);
            UserRequest newUser = new UserRequest();
            newUser.setEmail("puru@gmail.com");
            newUser.setUserName("puru");
            newUser.setPassword("123123");
            userService.save(newUser);
            LOGGER.warn("USER IS SAVED");
        } else {
            LOGGER.warn("USER IS ALREADY THERE");
        }
    }

}
