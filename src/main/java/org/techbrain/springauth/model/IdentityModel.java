package org.techbrain.springauth.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
public class IdentityModel implements Serializable {

    @MongoId
    @MongoObjectId
    @ApiModelProperty(notes = "Auto-generated")
    private String id;

    @ApiModelProperty(notes = "Auto-generated")
    private long createdDate;

    @ApiModelProperty(notes = "Auto-generated")
    private long updatedDate;
    protected boolean deleted;
    public IdentityModel() {
    }

    public void prepare(){
        if(Objects.isNull(id)){
            this.createdDate =  new Date().getTime();
            this.deleted = false;
        }else {
            this.updatedDate = new Date().getTime();
        }
    }
}
