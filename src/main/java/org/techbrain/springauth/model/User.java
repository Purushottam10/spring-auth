package org.techbrain.springauth.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.techbrain.springauth.config.CollectionName;
import org.techbrain.springauth.request.UserRequest;


@Getter
@Setter
@CollectionName(name = "user")
@NoArgsConstructor
public class User extends IdentityModel{
    private String userName;
    private String password;
    private String email;
    public User(UserRequest userRequest){
        this.userName = userRequest.getUserName();
        this.password = userRequest.getPassword();
        this.email = userRequest.getEmail();
    }
}
